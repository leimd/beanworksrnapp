// Where CombineReducers Lives and default state lives
// @flow

import { combineReducers } from 'redux'
import user from './User/UserReducers';
import {NavReducer} from '../Screens/AppNavigatorWithState';

export default combineReducers({
        nav: NavReducer,
        user
    });