/**
 * Created by beanuser
 * Beanworks 2017
 */

import BlankScreen from './BlankScreen';
import LoginScreen from './LoginScreen';

export const initialNavState = {
    index: 0,
    routes: [{
        routeName: 'Login',
        key: 'Test-1'
    }]
};

export default AppRouteConfigs = {
    Test: {
        screen: BlankScreen
    },
    Login: {
        screen: LoginScreen
    }
};