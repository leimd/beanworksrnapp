// @flow
import { LOGIN } from "../Actions";

export default function UserLoginAction(usernameAndPassword: Object) {
  return function(dispatch: function, getState: function) {
    dispatch({ type: LOGIN, payload: usernameAndPassword });
  };
}
