# Packing Slip App

## Up and Running
    1.Download Exponent on expo.io
    2.clone the repo
    3.Run with Exponent and view it on your phone
    
# Stack choice

React Native

Testing: Jest & Enzyme

Data Layer: ImmutableJS, Redux and Redux-thunk

Backbone Model: Immutable.Record
Backbone Collection: Immutable.List

Navigation: react-navigation

Type Checker: Flow

Befauifier: Prettier

# Reading List
[React Native App Stack, March 2017](https://medium.com/react-native-development/react-native-app-stack-march-2017-f7605e02d46f#.p59c9p9va)

[Coding Apps with React Native at Exponent](https://blog.expo.io/coding-apps-with-react-native-at-exponent-7a5922da27bf#.bw727ovaw)

[React-navigation with redux and immutable.js](https://medium.com/the-react-native-log/react-navigation-with-redux-and-immutable-js-1385c0457cb8#.5ka65tqab)

[Immutable.js Records with React and Redux](https://medium.com/azendoo-team/immutable-record-react-redux-99f389ed676#.k3zjsvyzi)

#Reference
[Redux](http://redux.js.org/docs/introduction/)

[Redux-Thunk](https://github.com/gaearon/redux-thunk)

[React-Navigatoion](https://reactnavigation.org/docs/guides)