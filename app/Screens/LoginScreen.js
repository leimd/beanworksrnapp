// @flow
import React from 'react';
import {View, Text, StyleSheet, TextInput, Button} from 'react-native';
import {connect} from 'react-redux';
import ColumnContainer from '../Components/ColumnContainer';

class LoginScreen extends React.Component {
    constructor(...args) {
        super(...args);
        this.state = {
            username: '',
            password: ''
        };
    }

    login() {
        debugger;
    }

    render() {
        return (
            <ColumnContainer style={styles.container}>
                <View style={styles.textInputBoxCombo}>
                    <Text style={styles.text}> Username</Text>
                    <TextInput
                        style={styles.textbox}
                        onChangeText={(username) => this.setState({username})}
                        placeHolder='Username'
                        underlineColorAndroid={'white'}
                    />
                </View>
                <View style={styles.textInputBoxCombo}>
                    <Text style={styles.text}> Username</Text>
                    <TextInput
                        style={styles.textbox}
                        onChangeText={(password) => this.setState({password})}
                        placeHolder='Password'
                        secureTextEntry={true}
                        underlineColorAndroid={'white'}
                    />
                </View>
                <View style={[styles.margin10AndPadding10, styles.fullWidthButton]}>
                <Button
                    onPress={this.login.bind(this)}
                    title="Login"
                    color='#388E3C'/>
                </View>
            </ColumnContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#2196F3'
    },
    textInputBoxCombo: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 20,
        marginBottom: 10
    },
    textbox: {
        flex: 1,
        marginLeft: 10,
        paddingLeft: 10,
        paddingBottom: 5,
        fontSize: 15,
        fontWeight: 'bold',
        color: 'white'
    },
    text: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 15
    },
    margin10AndPadding10: {
        paddingHorizontal: 10,
        marginTop        : 10,
    },
    fullWidthButton: {
        alignSelf        : 'stretch',
        alignItems       : 'stretch',
        flexDirection    : 'column'
    }
});

const mapStateToProps = function (state) {
    return state;
};

const mapDispatchToProps = function () {
    return {};
};


export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
