/**
 * Created by beanuser
 * Beanworks 2017
 */
// @flow
import React from "react";
import AppRouteConfigs from "./AppRouteConfigs";
import {StackNavigator, addNavigationHelpers} from "react-navigation";
import {connect} from 'react-redux';
import {initialNavState} from './AppRouteConfigs';

const AppNavigator = StackNavigator(AppRouteConfigs);

export const NavReducer = function(state: Object = initialNavState, action: string) {
    const newState = AppNavigator.router.getStateForAction(action, state);
    return newState || state;
};

class AppNavigatorWithState extends React.Component {
    render() {
        return (
            <AppNavigator
                navigation={addNavigationHelpers({
                    dispatch: this.props.dispatch,
                    state: this.props.nav,
                })}
            />
        );
    }
}

export default connect(function(state) {
    return {nav: state.nav}
})(AppNavigatorWithState);