/**
 * Created by beanuser
 * Beanworks 2017
 */
// @flow

import React from 'react';
import {View, StyleSheet} from 'react-native';

export default class ColumnContainer extends React.Component {
    render () {
        return (
            <View
                style = {[styles.container, this.props.style]}
            >
                {this.props.children}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
    }
});