import Expo from "expo";
import React from "react";
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import reducers from "./app/Reducers/Reducers";
import AppNavigatorWithState from "./app/Screens/AppNavigatorWithState";

const store = createStore(reducers, applyMiddleware(thunkMiddleware));

class App extends React.Component {
  render() {
    console.log('__DEV__ = ' + __DEV__);
    return (e
        <Provider store={store}>
          <AppNavigatorWithState/>
        </Provider>
    );
  }
}

// For StoryBook
// import App from './storybook'
// export default App

Expo.registerRootComponent(App);
